class Api::V1::UsersController < ApplicationController

	require 'json'
    before_filter :authorize, except: ["update","show","index"]


    def index
        @users = User.order(name: :asc)
        render 'index', status: 200
    end



    def show
            
        if params[:id].blank?
          return render json: {errors: 'user uuid not found'}, status: 503
        end
    
        unless UUID.validate(params[:id])
          return render json: {message: 'user uuid is not valid'}, status: 503
        end
    
        @user = User.find_by_uuid(params[:id])

        render 'show', status: 200
    end





	def update

        @user = User.find_by_uuid(params[:id])
	      if @user.nil?
	        render json: {errors: 'user not found'}, status: 404 
	      end

	      # if user_params[:avatar]
	      #   @user.avatar = Paperclip.io_adapters.for(user_params[:avatar])
	      # end

	      unless @user.update_attributes(user_params)
	         return render json: {errors: @user.errors.messages}, status: 422
	      end
        render 'show', status: 200
	end



	def destroy
	end

	private

	def user_params
		params.require(:user).permit(:name, :email, :picture, :genre)
	end
	
end
