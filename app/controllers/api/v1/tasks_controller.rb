class Api::V1::TasksController < ApplicationController

	require 'json'

	def index

	    @user = User.find_by_uuid(params[:user_id])
	     if @user.nil?
	          return render json: {errors: ['User not found']}, status: 404
	      end

	     @tasks = Task.owned_by(@user.id)

	     render 'index', status: 200
	end


	def show

		if params[:id].blank?
          return render json: {errors: 'task uuid not found'}, status: 503
        end
    
        unless UUID.validate(params[:id])
          return render json: {message: 'task uuid is not valid'}, status: 503
        end
    
        @task = Task.find_by_uuid(params[:id])


        render 'show', status: 200


	end



	def create

	  @user = User.find_by_uuid(params[:user_id])

       if @user.nil?
            return render json: {errors: ['User not found']}, status: 404
        end

       @task = Task.new(task_params)
       @task.user = @user

       unless @task.save
            return render json: {errors: @task.errors.messages }, status: 422
       end

      render 'create', status: 200

	end


	def update

	    @task = Task.find_by_uuid(params[:id])
	      if @task.nil?
	        render json: {errors: 'task not found'}, status: 404 
	      end

	      unless @task.update_attributes(task_params)
	         return render json: {errors: @task.errors.messages}, status: 422
	      end
	      
	    render 'show', status: 200

	end       


	def destroy

	   @task = Task.find_by_uuid(params[:id])
        if @task.nil?
          return render json: {errors: 'task not found'}, status: 404 
        end
        
        if @task.destroy
            render json: {message: "task #{params[:id]} was destroyed successfully"}, status: 200
        end

	end


	private 

	def task_params 
		params.require(:task).permit(:title, :desciption, :date, :notification)
	end

end
