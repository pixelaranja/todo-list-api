class Api::V1::SessionsController < ApplicationController
	
	require 'json'

    def create
      
      @account = User.find_by_email(sessions_params[:email])
    
      if !@account.nil?
        if @account.is_valid_password?(sessions_params[:password])
 
          @account.generate_access_token
          @account.save(validate: false)
  
          render 'create', status: 201
        else
            
          return render_auth_failed
        end
        
      else
          
        return render_account_not_found
      end
    end


  private
  
        def render_account_not_found
            render json: {error: {message: 'Account not found'}}, status: 404
        end
    
        def render_auth_failed
            render json: {error: {message: 'Authentication failed'}}, status: 401
        end

    def sessions_params
        params.require(:account).permit(:email, :password)
    end

end
