class Api::V1::AccountsController < ApplicationController
	
	require 'json'
    before_filter :authorize, except: ["create"]
    before_action :check_password_confirmation, only: [:create]


    def create
        @account = User.find_by_email(account_params[:email])
        unless @account

            @account = User.new(account_params)
            unless @account.save
                return render json: {errors: @account.errors.messages}, status: 422
            end
        end
        render 'create', status: 200
    end


    private

    def account_params
        params.require(:account).permit(:name, :email, :genre, :password, :password_confirmation, :uid, :picture)
    end

    def check_password_confirmation
        if account_params[:password] != account_params[:password_confirmation]
          return render json: {error: {message: 'unmatched password confirmation'}}, status: 422
        end
    end

end
