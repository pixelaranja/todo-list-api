class ApplicationController < ActionController::Base

	protect_from_forgery with: :null_session

   def authorize
      
       header_access_token = request.headers['X-Client-Access-Token']

       unless header_access_token.nil?
           @client = User.find_by_access_token(header_access_token)
        if !@client.nil?
          if !@client.ensure_token?(header_access_token)
            return render json: { message: 'Authorization denied.' }, status: 401
          end
          return true
        end
          return render json: { message: 'Client not found.' }, status: 404
        end

        return render json: { message: 'Authorization denied.' }, status: 401
    end

end
