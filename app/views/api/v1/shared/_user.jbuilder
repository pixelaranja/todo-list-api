json.id user.id
json.name user.name
json.email user.email
json.uuid user.uuid
json.genre user.genre
json.access_token user.access_token
json.picture user.picture
json.created user.created_at