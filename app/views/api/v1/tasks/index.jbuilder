json.tasks @tasks do |task|

	json.title task.title
	json.desciption task.desciption
	json.date task.date
	json.notification task.notification
	json.uuid task.uuid

	json.user do 
		json.partial! 'api/v1/shared/user', user: task.user
	end
end
