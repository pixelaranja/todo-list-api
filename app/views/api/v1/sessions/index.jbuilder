json.session do 
	json.name @account.name
	json.email @account.email 
	json.uuid  @account.uuid
	json.genre @account.genre
	json.uid @account.uid
	json.picture @account.picture
	json.access_token @account.access_token
end