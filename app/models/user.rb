class User < ActiveRecord::Base
	attr_accessor :password, :password_confirmation

    # validations
    validates :name, :email, presence: true
    validates :password, :password_confirmation, presence: true, :on => :create
    validates :email, uniqueness: true

     # filters
    before_save do
        self.email = self.email.try(:downcase)
        # sanitize_fields
        create_uuid
        create_security
    end


    #associations
    has_many :tasks


    def to_param
        uuid || id
    end
    
    
    def generate_access_token
      dynamic_data = SecureRandom.hex(64)
      self.access_token = Base64.encode64(Live::AESCrypt.encrypt("#{self.uuid}#{dynamic_data}", self.password_digest, nil, 'AES-256-ECB')).tr('+/','-_').gsub("\n", '')
    end
    
    def encrypt_password
      self.password_digest = Live::Encryptor.new(self.password).encrypt
    end
    
    def ensure_token?(header_access_token)
        decoded_access_token = clear_special_chars_before_decode64(header_access_token)
        client_uuid = decrypt_decoded_token(decoded_access_token)
        self.uuid == client_uuid[0..35]
    end

    def is_valid_password?(entered_password)
        password_object = BCrypt::Password.new(self.password_digest)
        encrypted_password = Live::Encryptor.new(entered_password).concat_salt_to_password
        password_object == encrypted_password
    end

    private
    
        # def sanitize_fields
        #   self.cpf = self.cpf.scan(/\d/).join unless self.cpf.nil?
        # end
    
        def create_uuid
            self.uuid = SecureRandom.uuid if self.uuid.blank?
        end
        
        def create_security
    
          unless self.password.nil? || self.password_confirmation.nil?
    
            if self.password_digest.blank?
              encrypt_password
            end
    
            if self.access_token.blank?
              generate_access_token
            end
    
          end
    
        end
        
        def clear_special_chars_before_decode64(access_token)
          Base64.decode64(access_token.tr('-_', '+/'))
        end
    
        def clear_special_chars_before_encode64(access_token)
          Base64.encode64(access_token).tr('+/','-_').gsub("\n", '')
        end
    
        def decrypt_decoded_token(decoded_access_token)
          Live::AESCrypt.decrypt(decoded_access_token, self.password_digest, nil, 'AES-256-ECB')
        end
    
        def create_access_token
          Live::AESCrypt.encrypt(self.uuid, self.password_digest, nil, 'AES-256-ECB')
        end

end
