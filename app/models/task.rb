class Task < ActiveRecord::Base

	before_save :create_uuid 

    # scopes
    scope :owned_by, -> (id) { where(user_id: id).order('id desc') }


    #associations
    belongs_to :user

    scope :owned_by, -> (id) { where(user_id: id).order('id desc') }


    def to_param
        uuid || id
    end

    private

    def create_uuid
         self.uuid = SecureRandom.uuid if self.uuid.blank?
    end

end
