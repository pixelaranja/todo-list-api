class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|

      t.string :title
      t.string :desciption
      t.datetime :date
      t.boolean :notification, default: false

      t.timestamps null: false
    end
  end
end
