class AddUuidToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :uuid, :uuid
  	add_column :users, :uid, :string
  end
end
