Rails.application.routes.draw do

  root "home#index"

  namespace :api, path: 'api' do
       namespace :v1 do 

          resources :sessions, only: [:create]
          resources :accounts, only: [:create]
          
          resources :users do 
            resources :tasks, only: [:create,:index]
          end
            resources :tasks,  only: [:destroy,:update]
       end
   end

end
