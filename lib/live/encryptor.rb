require 'bcrypt'

module Live

  class Encryptor

    include BCrypt

    def initialize(password)
      @password = password
      @salt = YAML.load_file("#{Rails.root.to_s}/config/secrets.yml")[Rails.env]["password_salt"]
    end

    def encrypt
      BCrypt::Password.create(concat_salt_to_password)
    end

    def concat_salt_to_password
      "#{@password}#{@salt}"
    end

  end

end